#Sifat Jahir - 14113804 - CMP4266 Computer Programming
from account import Account

#Subclass of Account class
class CurrentAccount(Account):

    #Constructor method
    def __init__(self, balance, account_no, repay_date, interest_rate, overdraft_limit):

        #Inherits parameters from the parent class
        super().__init__(balance, account_no, repay_date, interest_rate, overdraft_limit)

    #Get account type
    def get_account_type(self):
        print("Current Account")


