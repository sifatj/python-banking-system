#Sifat Jahir - 14113804 - CMP4266 Computer Programming
from person import Person # Import Person class

#Subclass of the Person class
class Admin(Person):

    #Constructor
    def __init__(self, name, password, full_rights, address = [None, None, None, None]):
        super().__init__(name, password, address)
        self.full_admin_rights = full_rights
        
    #FChecks if admin can delete customers.
    def has_full_admin_right(self):
        return self.full_admin_rights

