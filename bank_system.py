#Sifat Jahir - 14113804 - CMP4266 Computer Programming

#Delete the pickle files if any issues occur. This usually fixes the problem.

#import classes
from customer import Customer
from admin import Admin
from currentaccount import CurrentAccount
from savingsaccount import SavingsAccount

import datetime 
import pickle 
import os 
    
class BankSystem(object):
    def __init__(self):
        self.customers_list = []
        self.admins_list = []
        self.load_bank_data()
        self.current_date = datetime.date.today() #Gets the current date


    #Loads the bank data with customer, account, and admin information
    def load_bank_data(self):
        PATH = './personData.pkl'
        customerData = 'personData.pkl'
        if os.path.isfile(PATH) and os.access(PATH, os.R_OK):
            #Open the pickle file and read the customers data from it
            with open(customerData, "rb") as f: # Open in read binary mode
                #Load the pickle data into lists
                self.customers_list = pickle.load(f) 
                self.admins_list = pickle.load(f)
        
        else:
            with open(customerData, "wb") as f: # Open in write binary mode
            #Create customer, account, and admin objects from this data if pickle file doesnt exist

                #Customer 1 - Adam
                #Create Customer object with given parameters
                customer_1 = Customer("Adam", "1234", ["14", "Wilcot Street", "Bath", "B5 5RT"])
                account_no = 1234

                #Create CurrentAccount object with given parameters
                account_1 = CurrentAccount(5001.00, account_no,"",0.9,500)
                account_no += 1 #add 1 to the current account_no

                #Create SavingsAccount object with given parameters
                account_savings_1 = SavingsAccount(523.00, account_no,"",0.9,500)

                #Add account objects to customer_accounts list using add_account() method
                customer_1.add_account(account_1) #Add account to the customer
                customer_1.add_account(account_savings_1)

                #Add the Customer object to the customers_list
                self.customers_list.append(customer_1)
                
                #Customer 2 - David
                customer_2 = Customer("David", "password", ["60", "Holborn Viaduct", "London", "EC1A 2FD"])
                account_no+=1
                account_2 = CurrentAccount(3200.00,account_no,"",0.9,500)
                account_no += 1
                account_savings_2 = SavingsAccount(5003.00, account_no,"",0.9,500)
                customer_2.add_account(account_2)
                customer_2.add_account(account_savings_2)
                self.customers_list.append(customer_2)

                #Customer 3 - Alice
                customer_3 = Customer("Alice", "MoonLight", ["5", "Cardigan Street", "Birmingham", "B4 7BD"])
                account_no+=1
                account_3 = CurrentAccount(18000.00,account_no,"",0.9,500)
                account_no += 1
                account_savings_3 = SavingsAccount(523.00, account_no,"",0.9,500)
                customer_3.add_account(account_3)
                customer_3.add_account(account_savings_3)
                self.customers_list.append(customer_3)

                #Customer 4 - Ali
                customer_4 = Customer("Ali", "150A",["44", "Churchill Way West", "Basingstoke", "RG21 6YR"])
                account_no+=1
                account_4 = CurrentAccount(40.00,account_no,"",0.9,500)
                account_no += 1
                account_savings_4 = SavingsAccount(1200.00, account_no,"",0.9,500)
                customer_4.add_account(account_4)
                customer_4.add_account(account_savings_4)

                #Adds the Customer objects to a list
                self.customers_list.append(customer_4)

                #Dumps the data into a pickle file. 
                pickle.dump(self.customers_list,f)
                
                #Admins
                #Admin 1 - Julian - Has full admin rights. Can delete customers.
                admin_1 = Admin("Julian", "1441", True, "12, London Road, Birmingham, B95 7TT")
                self.admins_list.append(admin_1) # Add Admin object to a list


                #Admin 1 - Eva - Cannot delete customers.
                admin_2 = Admin("Eva", "2222", False, "47, Mars Street, Newcastle, NE12 6TZ")
                self.admins_list.append(admin_2) # Add Admin object to a list

                #Dumps the admin list data into a pickle file.
                pickle.dump(self.admins_list,f)


    
    #Save function used to save the data into a pickle file format. Allows data to be retreived when system is reopened.            
    def save_bank_data(self):        
        PATH ='./personData.pkl'
        data ='personData.pkl'
        
        if os.path.isfile(PATH) and os.access(PATH, os.R_OK):
            with open(data, "wb") as f:# Open in write binary mode
                #Dump the customer and admin data into the pickle file
                pickle.dump(self.customers_list,f)
                pickle.dump(self.admins_list,f)
                print("Data has been saved")
        else:
            print("The data did not save")

        
    
    #Method used to search for customer
    def customer_login(self, name, password):
        #STEP A.1
        found_customer = self.search_customers_by_name(name)
        if found_customer == None:
            return("\n The customer has not been found!\n")
        else:
            if (found_customer.check_password(password) == True): #If password mactches
                
                self.run_customer_options(found_customer) # run this method
            else:
                return("you have input a wrong password")
    #Search method used to search for customer in customers_list
    def search_customers_by_name(self, customer_name):
        #STEP A.2
        found_customer = None
        for a in self.customers_list:
            name = a.get_name()
            if name == customer_name:
                found_customer = a
                break
        if found_customer == None: # If customer doesnt exist
                print("\nThe customer %s does not exist! Try again...\n" %customer_name)
        return found_customer


    #Main Menu prints login options
    def main_menu(self):
        #print the options you have
        print()
        print()
        print ("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print ("Welcome to the Python Bank System")
        print ("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print ("Current date: %s" %self.current_date) # gets the current date
        print ("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print ("1) Admin login")
        print ("2) Customer login")
        print ("3) Quit Python Bank System")
        print (" ")
        option = self.option_validation(1,3) #Option validation
        return option
    
    #Method  called when bank system starts
    def run_main_option(self):
        loop = 1         
        while loop == 1:
            choice = self.main_menu()
            if choice == 1:
                name = input ("\nPlease input admin name: ")
                password = input ("\nPlease input admin password: ")
                msg = self.admin_login(name, password)
                print(msg)
            elif choice == 2:
                name = input ("\nPlease input customer name: ")
                password = input ("\nPlease input customer password: ")
                msg = self.customer_login(name, password)
                print(msg)
            elif choice == 3:
                self.save_bank_data() #Saves bank data to pickle file
                loop = 0
        print ("Thank-You for stopping by the bank!")

        
    #Method used for transferring money from a sender to a receiver. An account has to be selected to transfer the money from.
    def transfer_money(self, account):
        #Get the sender name and search for them
        sender_name = input("\nPlease input the sender name :\n")
        sender = self.search_customers_by_name(sender_name)
        if sender == None: # Print message if sender does not exist
            return("\n The sender has not been found!\n")
        else:#Input sender password if name has been found
            sender_password = input("\nPlease input your password: ")
            if (sender.check_password(sender_password) == True):#If password matches

                #Use choose_account() method to choose an account to send money from
                sender_account = self.choose_account(sender)
                #If sender exists
                if sender_account != None:  
                    #Input the receivers name and use search method to search for them
                    receiver_name = input("\nPlease input the receiver name :\n")
                    receiver = self.search_customers_by_name(receiver_name)
                    #If receiver exists
                    if receiver != None:
                        #Choose the receiver account to send money to
                        receiver_account = self.choose_account(receiver)
                        #If receiver account exists
                        if receiver_account != None:
                            #Input the amount to be transferred
                            amount = float(input("::\nPlease enter amount to be deposited\n: "))
                            #If amount is greater than 0 and less than the senders current balance
                            if amount > 0:
                                if amount < sender_account.get_balance():
                                    sender_account.transfer(amount) #Use transfer function to remove amount from account
                                    receiver_account.deposit(amount) # Use deposit function to add amount to account
        
                                    print(str(amount), "has been deposited to %s" %receiver_name)
                                    self.save_bank_data() # Save the bank data to pickle file
                                else:
                                    print("Please select a lower amount")  
                                    
                            else:
                                print("Enter an amount greater than 0")
            else:
                print("You have input a wrong password")



 
    #Select an account for a customer.
    def choose_account(self, customer):
        self.print_accounts(customer) #Prints the account information for each account that a customer has
        while True:
            try:
                chosen_account = int(input("Select an account:"))
                if chosen_account != None:#If user enters an input
                    # Search for account to check if it exists. 
                    current_account = self.search_customer_own_accounts(customer, chosen_account)
                    return current_account #Return the selected account
                else:
                    print("Account doesnt exist")
            except ValueError: #Validation to inform users to enter valid input
                print("Please enter a valid account number")

    #Search for customers accounts. Contains parameters for inputting the customer and the account chosen
    #Method is called in choose_account() method
    def search_customer_own_accounts(self, customer, chosen_account):
        found_customer = None # Set to None type initially
        #Check in customer_accounts list
        for a in customer.customer_accounts:
            account = a.get_account_no() # Store account number in account variable
            if account == chosen_account: # If account number is the chosen account
                found_customer = a
                break
        if found_customer == None: #If customer account cannot be found
                print("\nThe customer %s does not exist! Try again...\n" %customer)
        return found_customer #Returns selected customer



    
    #Print transaction options for customers and getting input from user. Used by the run_customer_options() method
    def customer_menu(self, customer_name):
        #print the options you have
         print (" ")
         print ("Welcome %s : Your transaction options are:" %customer_name)#Displays the customers name
         print ("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
         print ("1) Transfer money")
         print ("2) Other account operations")
         print ("3) Profile settings")
         print ("4) Switch accounts")
         print ("5) Sign out")
         print (" ")
         option = self.option_validation(1,5)# Validation used to check if erroneous data and invalid input is entered
         return option # returns entered input
        
    
    def run_customer_options(self, customer):
        #Uses the choose_account() method to get the selected account          
        account=self.choose_account(customer)
        loop = 1 # Set to 1 initially
        while loop == 1:
            choice = self.customer_menu(customer.get_name())#Gets the transaction options menu for selected customer
            if choice == 1: # transfer money
                account.print_balance()
                self.transfer_money(account)
                account.print_balance()
            elif choice == 2: # other account operations
                account.run_account_options()
            elif choice == 3: # profile settings
                customer.run_profile_options()
            elif choice == 4: 
                account = self.choose_account(customer)
            elif choice == 5: # sign out
                self.save_bank_data()#Save the data onto a pickle file
                loop = 0
        print ("Exit account operations")

    #Admin login function that uses the name and password input from run_main_option method to search for admin in the admins_list        
    def admin_login(self, name, password):
        # STEP A.3
        found_admin = self.search_admin_by_name(name)
        if found_admin == None:
            return("\n The admin has not been found!\n")
        else:
            if (found_admin.check_password(password) == True):#If password is correct then run admin options
                self.run_admin_options(found_admin)
            else:
                return("You have input a wrong password")

    #Search for admin in admins_list
    def search_admin_by_name(self, admin_name):
        # STEP A.4
        found_admin = None #Set to None initially
        for a in self.admins_list:
            name = a.get_name()
            if name == admin_name: # If name matches
                found_admin = a
                break # Break out of the conditonal statements
        if found_admin == None: # If admin cannot be found
                print("\nThe admin %s does not exist! Try again...\n" %admin_name)
        return found_admin

    #Print main menu options for admins and getting input from user. Used by the run_admin_options() method
    def admin_menu(self, admin_name):
        #print the options you have
         print (" ")
         print ("Welcome Admin %s : Avilable options are:" %admin_name)#Displays the admins name
         print ("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
         print ("1) Customer account operations")
         print ("2) Customer profile settings")
         print ("3) Admin profile settings")
         print ("4) Delete customer")
         print ("5) Print all customers detail")
         print ("6) Sign out")
         print (" ")
         option = self.option_validation(1,6)# Validation used to check if erroneous data and invalid input is entered
         return option # returns selected input

    #Method that used the input from admin_menu() method.
    def run_admin_options(self, admin):                       
        loop = 1 # Set to 1 initially
        while loop == 1:
            choice = self.admin_menu(admin.get_name())#Gets the admin main menu for selected admin
            if choice == 1: # Customer account operations
                #STEP A.5
                customer_name = input("\nPlease input customer name :\n")
                customer = self.search_customers_by_name(customer_name)
                if customer != None:
                    account = self.choose_account(customer) #Gets the account for seleted customer
                    if account != None: #If account exists then run account options
                        account.run_account_options()

            elif choice == 2:#Customer profile settings
                #STEP A.6
                customer_name = input("\nPlease input customer name :\n")
                customer = self.search_customers_by_name(customer_name)
                if customer != None: #Runs profile options for selected customer if customer exists
                    customer.run_profile_options()

            elif choice == 3:#Admin profile settings
                #STEP A.7
                admin.run_profile_options() # Run this method. Displays the profile options for admins

            elif choice == 4:#Delete customer
                #STEP A.8
                if admin.has_full_admin_right() == True: #Allows only admins with full rights to delete customer. This is specified when creating the admin object
                    customer_name = input("\nPlease input customer name you want to delete :\n")
                    customer = self.search_customers_by_name(customer_name) #Search for inputted name in customers_list
                    if customer == None: 
                        print(" ")   
                    elif customer != None: #If input has been entered
                        self.customers_list.remove(customer)#Remove Customer object from list
                        self.save_bank_data()#Save data to pickle file
                else:#Print message if admin does not have full rights.
                    print("\nOnly administrators with full admin rights can remove a customer from the bank system!\n")

            elif choice == 5:#Print all customers detail
                #STEP A.9
                self.print_all_accounts_details()
            elif choice == 6:#Sign out
                self.save_bank_data()
                loop = 0 #Break out of the loop
        print ("Exit account operations")

    #Method used by admins to print details for all customers. This includes their name, address, balance, and all accounts
    def print_all_accounts_details(self):
        i = 0
        for c in self.customers_list: #Customers in the customers_list
            i+=1
            print('\n %d. ' %i, end = ' ')
            c.print_details()
            self.print_accounts(c) # Calls the print_accounts method to print all account details for each customer
            print("------------------------")

    #Prints all the different account details for a customer
    def print_accounts(self, customer):
        i = 0
        for c in customer.customer_accounts:#Accounts appended to customer_accounts list in Customer class
            i+=1
            print('\n %d. ' %i, end = ' ')
            c.print_account_details()
            print("------------------------")

    #Used for error handling on menu options. Stops users from inputting erroneous data
    #Min and Max parameters are used for specifying the range of options that can be selected
    def option_validation(self, min, max):
        while True:
            try:
                option = int(input("Select an option: "))
                #If number is under or over the specified range, print message
                if option < min or option > max:
                    print("Please select a valid option")
                else:
                    return option # Returns selected input
            except ValueError:#If erroneous data has been inputted, print a message
                print("Please select a number")


if __name__ == '__main__':
    app = BankSystem() #Create BankSystem object
    app.run_main_option()
