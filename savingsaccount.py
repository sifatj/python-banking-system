#Sifat Jahir - 14113804 - CMP4266 Computer Programming
from account import Account

#Subclass of Account class
class SavingsAccount(Account): #Inherit from Account class

    #Constructor method
    def __init__ (self, balance, account_no, repay_date, interest_rate, overdraft_limit):
        #Inherits below parameters from the parent class
        super().__init__(balance, account_no, repay_date, interest_rate, overdraft_limit)

    #Get account type
    def get_account_type(self):
        print("Savings Account")
        
    #Stops customer from withdrawing from a savings account.
    #Original withdraw function overwritten with this one.
    def withdraw(self, amount):
        print("Money cannot be withdrawn from savings account")
