#Sifat Jahir - 14113804 - CMP4266 Computer Programming
from person import Person

#Subclass of Person class
class Customer(Person): 
    
    #Constructor
    def __init__(self, name, password, address = [None, None, None, None]):
        super().__init__(name, password, address)
        # List used for storing all accounts for each customer
        self.customer_accounts = [] 

    #Add multiple accounts to each Customer object
    def add_account(self, account):
        self.customer_accounts.append(account)

    #Overwrites parent class method 
    def print_details(self):
        #Inherits the print_details method from the Person class
        super().print_details()
        print(" ")
