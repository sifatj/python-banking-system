#Sifat Jahir - 14113804 - CMP4266 Computer Programming

import random 
import datetime 

class Account(object):

    def __init__(self, balance, account_no, repay_date, interest_rate, overdraft_limit):
        self.balance = float(balance)
        self.account_no = int(account_no)
        self.interest_rate = float(interest_rate)
        self.overdraft_limit = int(overdraft_limit)
        self.repay_date = str(repay_date)

    #Get balance
    def get_balance(self):
        return self.balance
    
    #Get account number
    def get_account_no(self):
        return self.account_no
    
    #Get interest rate
    def get_interest_rate(self):
        return self.interest_rate
    
    #Get overdraft limit
    def get_overdraft_limit(self):
        return self.overdraft_limit
    
    #Deposit money into account
    def deposit(self, amount):
        if amount > 0:
            print("You have deposited £" + str(amount))
            self.balance+=amount
        else:
            print("Your deposit must be greater than £0")
            
    #Withdraw money from account
    def withdraw(self, amount):
        if amount > 0:
            if amount < self.balance: # Check that amount is less than current balance
                print("You have withdrawn £" + str(amount))
                self.balance -= amount
            else:
                print("You do not have enough funds to withdraw that")
        else:
            print("Withdraw an amount greater than £0 please")
            
    #Transfer money from account
    def transfer(self, amount):
        if amount > 0:
            if amount < self.balance: # Check that amount is less than current balance
                print("You have transferred £" + str(amount))
                self.balance -= amount
            else:
                print("You do not have enough funds to transfer that")
        else:
            print("Tranfer must be an amount over £0")
            
    #Prints current balance
    def print_balance(self):
        print("Your account balance is %.2f" %self.balance)

    #Print account type, account number, and balance for a customer
    def print_account_details(self):
        self.get_account_type()
        bal = self.get_balance()
        acc_no = self.get_account_no()
        print("Account balance: %.2f" %bal)
        print("Account number: %s" %acc_no)
            
    
    #Check if loan can be granted.  
    def check_loan(self):
        loan = False
        rand = random.randrange(100)
        #30 percent probability of it being accepted
        if rand <= 30:
            loan = True
        else:
            loan = False         
        return loan

    #Method used when requesting loan. Calls the check_loan() function first
    #to check if loan can be granted. Grants the loan if it is under the loan limit
    #and is also over 0. Method also calls the loan_date() method to get the date.
    def request_loan(self, loan_amount):
        loan_limit = 1000
        if self.check_loan(): # If loan accepted
            print("You are qualified to get a loan!")
            if loan_amount > 0: #Checks that input amount is over 0
                if loan_amount > loan_limit:
                    print("Please select a lower amount!")
                else:
                    print("Loan has been accepted")
                    self.deposit(loan_amount)
                    self.loan_date()
        else:
            print("Loan cannot be granted")

    #Generate a loan date. Get the current date and the repayment date        
    def loan_date(self):
        current_date = datetime.date.today()
        #Calculate repayment date by add loan due date with current date
        repay_date = current_date + datetime.timedelta(days=21)
        #Used to convert the repayment date into string format
        self.repay_date= repay_date.strftime('%m/%d/%y')

        daily_fee = 20 # deduct this fee from balance
        #If the date has surpassed the repayment date, add a daily fee
        if current_date > repay_date:
            daily_fee += 20
            self.balance - daily_fee
        print("Loan granted on:", current_date)
        print("Loan must be repayed by:", repay_date)
        

    #Clear the loan amount.
    def clear_loan(self, amount):
        if amount > 0: #Checks if input amount is greater than 0
            if amount < self.balance: # If amount is not over current balance
                print("You have paid £" + str(amount))
                self.balance -= amount # Deduct amount from balance
            else:
                print("You do not have enough funds to clear this loan")
        else:
            print("Select an amount greater than £0")

    #Account menu options for users to select. 
    def account_menu(self):
        #print the options you have
        print (" ")
        print ("Your Transaction Options Are:")
        print ("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print ("1) Deposit money")
        print ("2) Check balance")
        print ("3) Withdraw money")
        print ("4) Request loan")
        print ("5) Clear loan")
        print ("6) Back")
        print (" ")
        option = self.option_validation(1,6)
        return option 

    #Call the account_menu() method and use its input.
    def run_account_options(self):
        loop = 1 # Sets loop to 1 initially
        while loop == 1:
            choice = self.account_menu() # Call method while loop is still 1
            if choice == 1:#Deposit
                try:
                    amount=int(input("::\nPlease enter amount to be deposited\n: "))
                    deposit = self.deposit(amount)
                    self.print_balance()
                except ValueError:
                    print("Input valid amount")
            elif choice == 2:#Check balance
                balance = self.print_balance()
            elif choice == 3:#Withdraw
                try:
                    amount=int(input("::\nPlease enter amount to be withdrawn\n: "))
                    withdraw = self.withdraw(amount)
                    self.print_balance()
                except ValueError:
                    print("Input valid amount")
            elif choice == 4:#Request loan
                try:
                    amount=int(input("::\nPlease enter loan amount\n: "))
                    self.request_loan(amount)
                    self.print_balance()
                except ValueError:
                    print("Input valid amount")
            elif choice == 5:#Clear loan
                try:
                    amount = int(input("::\nPlease enter loan amount that you are paying\n: "))
                    self.clear_loan(amount)
                    self.print_balance()
                except ValueError:
                    print("Input valid amount")
            elif choice == 6:#Back
                loop = 0 # Exit the loop
        print ("Exit account operations")

    #Error handling on menu options
    def option_validation(self, min, max):
        while True:
            try:
                option = int(input("Select an option: "))
                #If number is under or over the specified range, print message
                if option < min or option > max:
                    print("Please select a valid option")
                else:
                    return option # Returns selected input
            #If erroneous data has been inputted, print a message
            except ValueError:
                print("Please select a number")
