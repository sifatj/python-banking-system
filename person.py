#Sifat Jahir - 14113804 - CMP4266 Computer Programming
class Person(object):
    
    #Constructor method
    def __init__(self, name, password, address = [None, None, None, None]):
        self.name = name
        self.password = password
        self.address = address
        
    #Set name (setter method)
    def update_name(self, name):
        self.name = name
        
    #Get name
    def get_name(self):
        return self.name
    
    #Set password
    def update_password(self, password):
        self.password = password
        
    #Get password
    def get_password(self):
        return self.password
    
    #Set address
    def update_address(self, address):
        self.address = address
        
    #Get address
    def get_address(self):
        return self.address
    
    #Prints out name and address 
    def print_details(self):
        print("Name: %s" %self.name)
        print("Address: %s" %self.address[0]) # house number
        print("         %s" %self.address[1]) # street
        print("         %s" %self.address[2]) # town
        print("         %s" %self.address[3]) # postcode
        print(" ")
        

    #Check user password. Returns false if the passwords don't match
    def check_password(self, password):
        if self.password == password:
            return True
        else:
            return False

    #Validation of the menu options
    def choose_range(self, min, max):
        try:
            option = int(input("Select an option: "))
            if option < min or option > max:
                print("Please select a valid option")
            else:
                print("Option accepted")
                return option
        except ValueError:
            print("Please select a number")

    #Print profile options
    def profile_settings_menu(self):
         print (" ")
         print ("Your Profile Settings Options Are:")
         print ("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
         print ("1) Update name")
         print ("2) Print details")
         print ("3) Update address")
         print ("4) Update password")
         print ("5) Back")
         print (" ")
         return self.choose_range(1,5)

    #Uses input from profile_settings_menu method to work   
    def run_profile_options(self):
        loop = 1           
        while loop == 1:
            choice = self.profile_settings_menu()
            if choice == 1:
                name=input("\n Please enter new name\n: ")
                self.update_name(name)
                print("Name updated")
            elif choice == 2:
                self.print_details()
            elif choice == 3:
                address_1=input("\n Please enter a house number\n: ")
                address_2=input("\n Please enter a street name\n: ")
                address_3=input("\n Please enter a city name\n: ")
                address_4=input("\n Please enter a postcode\n: ")
                address_list=[address_1, address_2, address_3, address_4]
                self.update_address(address_list)
                print("Address updated")
            elif choice == 4:
                password=input("\n Please enter new password\n: ")
                self.update_password(password)
                print("Password updated")
            elif choice == 5:
                loop = 0                     
